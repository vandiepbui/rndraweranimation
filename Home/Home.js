import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Dimensions,
  Text,
  View,
  StyleSheet,
  Animated,
  TouchableWithoutFeedback,
  ScrollView,
  BackAndroid,
  TouchableOpacity
} from "react-native";

import styles from "./styles";

const menus = [
  {
    text: "Menu item 1"
  },
  {
    text: "Menu item 2"
  },
  {
    text: "Menu item 3"
  },
  {
    text: "Menu item 4"
  },
  {
    text: "Menu item 5"
  },
  {
    text: "Menu item 6"
  },
  {
    text: "Menu item 7"
  },
  {
    text: "Menu item 8"
  },
  {
    text: "Menu item 9"
  },
  {
    text: "Menu item 10"
  }
];

export default class Home extends React.PureComponent {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);

    this.state = {
      activityLeftPos: new Animated.Value(0),
      scaleSize: new Animated.Value(1.0),
      rotate: new Animated.Value(0),
      animationDuration: 400,
      stagArr: [],
      animatedStagArr: [],
      menuItems: menus,
      activeMenu: 0,
      active: false
    };
  }

  // staggered animation configuration for menu items
  componentDidMount() {
    let stagArrNew = [];
    for (let i = 0; i < this.state.menuItems.length; i++) stagArrNew.push(i);
    this.setState({ stagArr: stagArrNew });

    let animatedStagArrNew = [];
    stagArrNew.forEach(value => {
      animatedStagArrNew[value] = new Animated.Value(0);
    });
    this.setState({ animatedStagArr: animatedStagArrNew });
  }

  // any update to component will fire the animation
  componentDidUpdate() {
    this._animateStuffs();

    if (this.props.handleBackPress && this.state.active) {
      BackAndroid.addEventListener(
        "hardwareBackPress",
        this._hardwareBackHandler
      );
    }

    if (this.props.handleBackPress && !this.state.active) {
      BackAndroid.removeEventListener(
        "hardwareBackPress",
        this._hardwareBackHandler
      );
    }
  }

  onMenuPress = () => {
    this.setState({ active: !this.state.active });
  };

  render() {
    const rotateVal = this.state.rotate.interpolate({
      inputRange: [0, 1],
      outputRange: ["0deg", "-60deg"]
    });

    const staggeredAnimatedMenus = this.state.stagArr.map(index => {
      const borderTopWidth = index == 0 ? 0.5 : 0;
      return (
        <TouchableWithoutFeedback
          key={index}
          onPress={this._handlePress.bind(this, index)}
        >
          <Animated.View
            style={{
              transform: [{ translateX: this.state.animatedStagArr[index] }]
            }}
          >
            <View style={[styles.menuItemContainer, { borderTopWidth }]}>
              <Text style={styles.menuItem}>
                {this.state.menuItems[index].text}
              </Text>
            </View>
          </Animated.View>
        </TouchableWithoutFeedback>
      );
    });

    return (
      <View
        style={[
          styles.offCanvasContainer,
          {
            flex: 1,
            backgroundColor: "#e1f3f7"
          }
        ]}
      >
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{
            position: "absolute",
            top: 0,
            left: 0,
            right: 0,
            bottom: 0
          }}
        >
          <Animated.View style={styles.menuItemsContainer}>
            {staggeredAnimatedMenus}
          </Animated.View>
        </ScrollView>

        <Animated.View
          onStartShouldSetResponder={() => true}
          onResponderTerminationRequest={() => true}
          onResponderRelease={evt => this._gestureControl(evt)}
          style={[
            styles.activityContainer,
            {
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "#00adee",
              transform: [
                { translateX: this.state.activityLeftPos },
                { scale: this.state.scaleSize },
                { rotateY: rotateVal }
              ]
            }
          ]}
        >
          <TouchableOpacity onPress={this.onMenuPress}>
            <Text style={styles.btn}>Open Menu</Text>
          </TouchableOpacity>
        </Animated.View>
      </View>
    );
  }

  // press on any menu item, render the respective scene
  _handlePress(index) {
    this.setState({ activeMenu: index });
    this.onMenuPress();
  }

  _hardwareBackHandler() {
    this.onMenuPress();
    return true;
  }

  // control swipe left or right reveal for menu
  _gestureControl(evt) {
    const { locationX, pageX } = evt.nativeEvent;

    if (!this.props.active) {
      if (locationX < 40 && pageX > 100) this.onMenuPress();
    } else {
      if (pageX) this.onMenuPress();
    }
  }

  // animate stuffs with hard coded values for fine tuning
  _animateStuffs() {
    const activityLeftPos = this.state.active ? 150 : 0;
    const scaleSize = this.state.active ? 0.8 : 1;
    const rotate = this.state.active ? 1 : 0;
    const menuTranslateX = this.state.active ? -50 : -250;

    Animated.parallel([
      Animated.timing(this.state.activityLeftPos, {
        toValue: activityLeftPos,
        duration: this.state.animationDuration
      }),
      Animated.timing(this.state.scaleSize, {
        toValue: scaleSize,
        duration: this.state.animationDuration
      }),
      Animated.timing(this.state.rotate, {
        toValue: rotate,
        duration: this.state.animationDuration
      }),
      Animated.stagger(
        50,
        this.state.stagArr.map(item => {
          if (this.state.active) {
            return Animated.timing(this.state.animatedStagArr[item], {
              toValue: menuTranslateX,
              duration: this.state.animationDuration,
              delay: 250
            });
          } else {
            return Animated.timing(this.state.animatedStagArr[item], {
              toValue: menuTranslateX,
              duration: this.state.animationDuration,
              delay: 400
            });
          }
        })
      )
    ]).start();
  }
}
