import EStyleSheet from "react-native-extended-stylesheet";

export default (styles = EStyleSheet.create({
  menuItemsContainer: {
    paddingTop: "80rem"
  },
  menuItemContainer: {
    justifyContent: "center",
    alignItems: "center",
    borderBottomWidth: 0.5,
    borderColor: "#ddd"
  },
  menuItem: {
    fontSize: "13rem",
    paddingTop: "10rem",
    paddingBottom: "10rem",
    color: "$primaryBlue"
  },
  btn: {
    color: "$white"
  }
}));
