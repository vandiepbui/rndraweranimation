# Custom drawer with animation

## Usage:

_- You just need clone this project and use it._

## Demo:

![](demo.gif)

## Note:

_Demo use library : react-native-extended-stylesheet. Please install it first before using demo code!_

## Reference:

_https://github.com/shoumma/react-native-off-canvas-menu_
